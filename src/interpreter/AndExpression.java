/**
 * @author Gabriele Cipolloni
 */
package interpreter;

/**
 * The Class AndExpression.
 */
public class AndExpression extends DoubleValueExpression {

	/**
	 * Instantiates a new and expression.
	 *
	 * @param expr1 the expr1
	 * @param expr2 the expr2
	 */
	public AndExpression(IExpression expr1, IExpression expr2) {
		super(expr1, expr2);
	}

	/* (non-Javadoc)
	 * @see interpreter.IExpression#interpret(java.lang.String)
	 */
	@Override
	public boolean interpret(String context) {
		return this.getExpr1().interpret(context) && this.getExpr2().interpret(context);
	}

}
