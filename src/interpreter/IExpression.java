/**
 * @author Gabriele Cipolloni
 */
package interpreter;

/**
 * The Interface IExpression.
 */
public interface IExpression {

	/**
	 * Interpret.
	 *
	 * @param context the context
	 * @return true, if successful
	 */
	boolean interpret(String context);

}
