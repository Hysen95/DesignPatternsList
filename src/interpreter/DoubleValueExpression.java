/**
 * @author Gabriele Cipolloni
 */
package interpreter;

/**
 * The Class DoubleValueExpression.
 */
public abstract class DoubleValueExpression implements IExpression {

	/** The expr2. */
	private IExpression expr1, expr2;

	/**
	 * Instantiates a new double value expression.
	 *
	 * @param expr1 the expr1
	 * @param expr2 the expr2
	 */
	public DoubleValueExpression(IExpression expr1, IExpression expr2) {
		this.expr1 = expr1;
		this.expr2 = expr2;
	}

	/**
	 * Gets the expr1.
	 *
	 * @return the expr1
	 */
	public IExpression getExpr1() {
		return this.expr1;
	}

	/**
	 * Gets the expr2.
	 *
	 * @return the expr2
	 */
	public IExpression getExpr2() {
		return this.expr2;
	}

	/**
	 * Sets the expr1.
	 *
	 * @param expr1 the new expr1
	 */
	public void setExpr1(IExpression expr1) {
		this.expr1 = expr1;
	}

	/**
	 * Sets the expr2.
	 *
	 * @param expr2 the new expr2
	 */
	public void setExpr2(IExpression expr2) {
		this.expr2 = expr2;
	}

}
