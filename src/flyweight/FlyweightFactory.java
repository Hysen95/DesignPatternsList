/**
 * @author Gabriele Cipolloni
 */
package flyweight;

import java.util.Map;
import java.util.TreeMap;

/**
 * A factory for creating Flyweight objects.
 */
public abstract class FlyweightFactory {

	/** The flyweights map. */
	private static Map<String, Flyweight> flyweightsMap;
	
	static {
		flyweightsMap = new TreeMap<>();
	}
	
	/**
	 * New flyweight.
	 *
	 * @param id the id
	 * @return the flyweight
	 */
	public static Flyweight newFlyweight(String id) {
		Flyweight flyweight = flyweightsMap.get(id);
		if (flyweight == null) {
			flyweight = new Flyweight(id);
			flyweightsMap.put(id, flyweight);
		}
		return flyweight;
	}
	
	/**
	 * Gets the flyweights map.
	 *
	 * @return the flyweights map
	 */
	public static Map<String, Flyweight> getFlyweightsMap() {
		return flyweightsMap;
	}
	
}
