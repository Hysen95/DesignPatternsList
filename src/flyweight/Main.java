/**
 * @author Gabriele Cipolloni
 */
package flyweight;

/**
 * The Class Main.
 */
public class Main {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		for (int i=0; i<10; i++) {
			FlyweightFactory.newFlyweight(String.valueOf(i));
		}
		for (int i=0; i<10; i++) {
			FlyweightFactory.newFlyweight(String.valueOf(i));
		}
		for (Flyweight flyweight : FlyweightFactory.getFlyweightsMap().values()) {
			System.out.println("\n Flyweight: "+flyweight);
		}
	}
	
}
