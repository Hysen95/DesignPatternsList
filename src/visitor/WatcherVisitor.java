/**
 * @author Gabriele Cipolloni
 */
package visitor;

import java.util.Collection;
import java.util.Iterator;

/**
 * The Class WatcherVisitor.
 */
public class WatcherVisitor implements Visitor {

	/* (non-Javadoc)
	 * @see visitor.Visitor#visit(visitor.VisitableString)
	 */
	@Override
	public void visit(VisitableString visitableString) {
		System.out.println(visitableString);
	}
	
	/* (non-Javadoc)
	 * @see visitor.Visitor#visit(visitor.VisitableFloat)
	 */
	@Override
	public void visit(VisitableFloat visitableFloat) {
		System.out.println(visitableFloat);
	}
	
	/* (non-Javadoc)
	 * @see visitor.Visitor#visit(java.util.Collection)
	 */
	@Override
	public void visit(Collection<?> list) {
		Iterator<?> iterator = list.iterator();
		while (iterator.hasNext()) {
			Object object = iterator.next();
			if (object instanceof Visitable) {
				((Visitable)object).accept(this);
			}
			else if (object instanceof Collection<?>) {
				visit((Collection<?>)object);
			}
		}
	}
	
}
