/**
 * @author Gabriele Cipolloni
 */
package visitor.reflection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The Class Main.
 */
public class Main {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		List<Object> list = new ArrayList<>(0);
		list.add("ciao");
		List<List<List<Object>>> l = new ArrayList<>(0);
		List<List<Object>> l3 = new ArrayList<>(0);
		List<Object> l2 = new ArrayList<>(Arrays.asList(3, 4, 5));
		l3.add(l2);
		l3.add(new ArrayList<>(Arrays.asList(6, 7, 8)));
		l.add(l3);
		list.add(l);
		List<Object> list2 = new ArrayList<>(0);
		list2.add(3.5F);
		list.add(list2);
		list.add(2.5F);
		list.add(2.5D);
		IReflectiveVisitor reflectiveVisitor = new ReflectiveVisitor();
		reflectiveVisitor.visit(list);
	}

}
