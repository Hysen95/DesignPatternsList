/**
 * @author Gabriele Cipolloni
 */
package visitor.reflection;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Iterator;

/**
 * The Class ReflectiveVisitor.
 */
public class ReflectiveVisitor implements IReflectiveVisitor {

	/**
	 * Default visit.
	 *
	 * @param object the object
	 */
	public void defaultVisit(Object object) {
		if (object instanceof Collection<?>) {
			this.visit((Collection<?>) object);
		}
		else {
			System.out.println(object.toString());
		}
	}

	/**
	 * Visit.
	 *
	 * @param collection the collection
	 */
	public void visit(Collection<?> collection) {
		Iterator<?> iterator = collection.iterator();
		while (iterator.hasNext()) {
			Object object = iterator.next();
			this.visit(object);
		}
	}

	/* (non-Javadoc)
	 * @see visitor.reflection.IReflectiveVisitor#visit(java.lang.Object)
	 */
	@Override
	public void visit(Object object) {
		try {
			Method method = this.getClass().getMethod("visit", new Class[] { object.getClass() });
			method.invoke(this, new Object[] { object });
		}
		catch (Exception e) {
			this.defaultVisit(object);
		}
	}

}
