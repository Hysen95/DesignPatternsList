/**
 * @author Gabriele Cipolloni
 */
package visitor.reflection;

/**
 * The Interface IReflectiveVisitor.
 */
public interface IReflectiveVisitor {

	/**
	 * Visit.
	 *
	 * @param object the object
	 */
	void visit(Object object);
	
}
