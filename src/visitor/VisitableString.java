/**
 * @author Gabriele Cipolloni
 */
package visitor;

/**
 * The Class VisitableString.
 */
public class VisitableString implements Visitable {

	/** The value. */
	private String value;
	
	/**
	 * Instantiates a new visitable string.
	 *
	 * @param value the value
	 */
	public VisitableString(String value) {
		this.value = value;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/* (non-Javadoc)
	 * @see visitor.Visitable#accept(visitor.Visitor)
	 */
	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}
	
}
