/**
 * @author Gabriele Cipolloni
 */
package visitor;

/**
 * The Interface Visitable.
 */
public interface Visitable {

	/**
	 * Accept.
	 *
	 * @param visitor the visitor
	 */
	void accept(Visitor visitor);
	
}
