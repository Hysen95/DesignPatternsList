/**
 * @author Gabriele Cipolloni
 */
package visitor;

/**
 * The Class VisitableFloat.
 */
public class VisitableFloat implements Visitable {

	/** The value. */
	private float value;
	
	/**
	 * Instantiates a new visitable float.
	 *
	 * @param value the value
	 */
	public VisitableFloat(float value) {
		this.value = value;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public float getValue() {
		return value;
	}
	
	/* (non-Javadoc)
	 * @see visitor.Visitable#accept(visitor.Visitor)
	 */
	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}
	
}
