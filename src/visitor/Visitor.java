/**
 * @author Gabriele Cipolloni
 */
package visitor;

import java.util.Collection;

/**
 * The Interface Visitor.
 */
public interface Visitor {

	/**
	 * Visit.
	 *
	 * @param visitableString the visitable string
	 */
	void visit(VisitableString visitableString);
	
	/**
	 * Visit.
	 *
	 * @param visitableFloat the visitable float
	 */
	void visit(VisitableFloat visitableFloat);
	
	/**
	 * Visit.
	 *
	 * @param list the list
	 */
	void visit(Collection<?> list);
	
}
