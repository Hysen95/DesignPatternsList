/**
 * @author Gabriele Cipolloni
 */
package visitor;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class Main.
 */
public class Main {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		List<Visitable> list = new ArrayList<>(0);
		list.add(new VisitableString("c"));
		list.add(new VisitableFloat(1.5F));
		Visitor browser = new WatcherVisitor();
		browser.visit(list);
	}
	
}
