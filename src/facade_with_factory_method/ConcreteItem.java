/**
 * @author Gabriele Cipolloni
 */
package facade_with_factory_method;

/**
 * The Class ConcreteItem.
 */
public class ConcreteItem implements Item {

	/* (non-Javadoc)
	 * @see facade_with_factory_method.Item#operation()
	 */
	@Override
	public void operation() {

	}

}
