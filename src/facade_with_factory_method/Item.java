/**
 * @author Gabriele Cipolloni
 */
package facade_with_factory_method;

/**
 * The Interface Item.
 */
public interface Item {

	/**
	 * Operation.
	 */
	void operation();

}
