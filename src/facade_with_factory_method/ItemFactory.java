/**
 * @author Gabriele Cipolloni
 */
package facade_with_factory_method;

/**
 * A factory for creating Item objects.
 */
public abstract class ItemFactory {

	/**
	 * Creates a new Item object.
	 *
	 * @return the item
	 */
	protected final Item createItem() {
		return this.newItem();
	}

	/**
	 * New item.
	 *
	 * @return the item
	 */
	public abstract Item newItem();

}
