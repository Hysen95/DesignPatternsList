/**
 * @author Gabriele Cipolloni
 */
package facade_with_factory_method;

/**
 * The Class StandardItemFacade.
 */
public class StandardItemFacade extends AbstractItemFacade {

	/** The item. */
	private Item item;

	/**
	 * Instantiates a new standard item facade.
	 *
	 * @param item the item
	 */
	public StandardItemFacade(Item item) {
		this.item = item;
	}

	/* (non-Javadoc)
	 * @see facade_with_factory_method.Item#operation()
	 */
	@Override
	public void operation() {
		System.out.println("\n Start Item Opertion. ");
		this.item.operation();
		System.out.println("\n End Item Opertion. ");
	}

}
