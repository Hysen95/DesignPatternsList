/**
 * @author Gabriele Cipolloni
 */
package facade_with_factory_method;

/**
 * A factory for creating StandardItem objects.
 */
public class StandardItemFactory extends ItemFactory {

	/* (non-Javadoc)
	 * @see facade_with_factory_method.ItemFactory#newItem()
	 */
	@Override
	public Item newItem() {
		return new StandardItemFacade(new ConcreteItem());
	}

}
