/**
 * @author Gabriele Cipolloni
 */
package facade_with_factory_method;

/**
 * The Class Main.
 */
public class Main {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		ItemFactory itemFactory = new StandardItemFactory();
		Item item = itemFactory.createItem();
		System.out.println(item);
	}

}
