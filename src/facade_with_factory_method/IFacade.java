/**
 * @author Gabriele Cipolloni
 */
package facade_with_factory_method;

/**
 * The Interface IFacade.
 */
public interface IFacade extends Item {

}
