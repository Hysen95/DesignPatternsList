/**
 * @author Gabriele Cipolloni
 */
package command;

/**
 * Example: PowerOffCommand, PowerOnCommand.
 */

public class ConcreteCommand extends AbstractCommand {

	/**
	 * Instantiates a new concrete command.
	 *
	 * @param context the context
	 */
	public ConcreteCommand(Context context) {
		super(context);
	}

	/* (non-Javadoc)
	 * @see command.ICommand#execute()
	 */
	@Override
	public void execute() {
		this.getContext().concreteCommand();
	}

}
