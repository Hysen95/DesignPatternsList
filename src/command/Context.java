/**
 * @author Gabriele Cipolloni
 */
package command;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * The Class Context.
 */
public class Context {

	/**
	 * Concrete command.
	 */
	public void concreteCommand() {
		System.out.println("\n concreteCommand() ");
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.MULTI_LINE_STYLE);
	}

}
