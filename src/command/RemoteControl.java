/**
 * @author Gabriele Cipolloni
 */
package command;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * The Class RemoteControl.
 */
public class RemoteControl {

	/** The commands map. */
	private Map<String, ICommand> commandsMap;
	
	/** The hystory. */
	private List<String> hystory;

	/**
	 * Instantiates a new remote control.
	 */
	public RemoteControl() {
		this.commandsMap = new TreeMap<>();
	}

	/**
	 * Adds the command.
	 *
	 * @param id the id
	 * @param command the command
	 */
	public void addCommand(String id, ICommand command) {
		this.commandsMap.put(id, command);
	}

	/**
	 * Execute command.
	 *
	 * @param id the id
	 */
	public void executeCommand(String id) {
		ICommand command = this.commandsMap.get(id);
		if (command != null) {
			this.hystory.add(id);
			command.execute();
		}
	}

}
