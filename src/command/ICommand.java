/**
 * @author Gabriele Cipolloni
 */
package command;

/**
 * The Interface ICommand.
 */
@FunctionalInterface
public interface ICommand {

	/**
	 * Execute.
	 */
	void execute();

}
