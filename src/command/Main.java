/**
 * @author Gabriele Cipolloni
 */
package command;

/**
 * The Class Main.
 */
public class Main {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		Context context = new Context();
		ICommand command = new ConcreteCommand(context);
		RemoteControl remoteControl = new RemoteControl();
		remoteControl.addCommand("1", command);
		remoteControl.executeCommand("1");
		remoteControl.addCommand("0", command);
		remoteControl.executeCommand("0");
	}

}
