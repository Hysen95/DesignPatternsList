/**
 * @author Gabriele Cipolloni
 */
package command;

/**
 * Wrapper for other commands.
 */

public abstract class AbstractCommand implements ICommand {

	/** The context. */
	private Context context;

	/**
	 * Instantiates a new abstract command.
	 *
	 * @param context the context
	 */
	public AbstractCommand(Context context) {
		this.context = context;
	}

	/**
	 * Gets the context.
	 *
	 * @return the context
	 */
	public Context getContext() {
		return this.context;
	}

	/**
	 * Sets the context.
	 *
	 * @param context the new context
	 */
	public void setContext(Context context) {
		this.context = context;
	}

}
