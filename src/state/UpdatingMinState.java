/**
 * @author Gabriele Cipolloni
 */
package state;

/**
 * The Class UpdatingMinState.
 */
public class UpdatingMinState extends ClockState {

	/**
	 * Instantiates a new updating min state.
	 *
	 * @param clock the clock
	 */
	public UpdatingMinState(Clock clock) {
		super(clock);
	}
	
	/* (non-Javadoc)
	 * @see state.ClockState#modeButton()
	 */
	@Override
	public void modeButton() {
		System.out.println("\n Mode: "+getClock().getClockState());
		getClock().setState(new NormalDisplayState(getClock()));
	}

	/* (non-Javadoc)
	 * @see state.ClockState#changeButton()
	 */
	@Override
	public void changeButton() {
		getClock().showTime();
	}
	
}
