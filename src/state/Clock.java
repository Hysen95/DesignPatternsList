/**
 * @author Gabriele Cipolloni
 */
package state;

import java.util.Date;

/**
 * The Class Clock.
 */
public class Clock {

	/** The clock state. */
	private ClockState clockState;
	
	/**
	 * Instantiates a new clock.
	 */
	public Clock() {
		clockState = new NormalDisplayState(this);
	}
	
	/**
	 * Sets the state.
	 *
	 * @param clockState the new state
	 */
	public void setState(ClockState clockState) {
		this.clockState = clockState;
	}
	
	/**
	 * Show time.
	 */
	public void showTime() {
		System.out.println(new Date());
	}
	
	/**
	 * Mode button.
	 */
	public void modeButton() {
		clockState.modeButton();
	}
	
	/**
	 * Change button.
	 */
	public void changeButton() {
		clockState.changeButton();
	}

	/**
	 * Gets the clock state.
	 *
	 * @return the clock state
	 */
	public ClockState getClockState() {
		return clockState;
	}

	/**
	 * Sets the clock state.
	 *
	 * @param clockState the new clock state
	 */
	public void setClockState(ClockState clockState) {
		this.clockState = clockState;
	}
	
}
