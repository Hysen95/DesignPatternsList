/**
 * @author Gabriele Cipolloni
 */
package state;

/**
 * The Class NormalDisplayState.
 */
public class NormalDisplayState extends ClockState {

	/**
	 * Instantiates a new normal display state.
	 *
	 * @param clock the clock
	 */
	public NormalDisplayState(Clock clock) {
		super(clock);
	}
	
	/* (non-Javadoc)
	 * @see state.ClockState#modeButton()
	 */
	@Override
	public void modeButton() {
		System.out.println("\n Mode: "+getClock().getClockState());
		getClock().setState(new UpdatingHrState(getClock()));
	}

	/* (non-Javadoc)
	 * @see state.ClockState#changeButton()
	 */
	@Override
	public void changeButton() {
		getClock().showTime();
	}
	
}
