/**
 * @author Gabriele Cipolloni
 */
package state;

/**
 * The Class UpdatingHrState.
 */
public class UpdatingHrState extends ClockState {

	/**
	 * Instantiates a new updating hr state.
	 *
	 * @param clock the clock
	 */
	public UpdatingHrState(Clock clock) {
		super(clock);
	}
	
	/* (non-Javadoc)
	 * @see state.ClockState#modeButton()
	 */
	@Override
	public void modeButton() {
		System.out.println("\n Mode: "+getClock().getClockState());
		getClock().setState(new UpdatingMinState(getClock()));
	}

	/* (non-Javadoc)
	 * @see state.ClockState#changeButton()
	 */
	@Override
	public void changeButton() {
		getClock().showTime();
	}
	
}
