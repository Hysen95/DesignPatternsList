/**
 * @author Gabriele Cipolloni
 */
package state;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * The Class ClockState.
 */
public abstract class ClockState {

	/** The clock. */
	private Clock clock;
	
	/**
	 * Instantiates a new clock state.
	 *
	 * @param clock the clock
	 */
	public ClockState(Clock clock) {
		this.clock = clock;
	}
	
	/**
	 * Gets the clock.
	 *
	 * @return the clock
	 */
	public Clock getClock() {
		return clock;
	}

	/**
	 * Sets the clock.
	 *
	 * @param clock the new clock
	 */
	public void setClock(Clock clock) {
		this.clock = clock;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	
	/**
	 * Mode button.
	 */
	public abstract void modeButton();
	
	/**
	 * Change button.
	 */
	public abstract void changeButton();
	
}
