/**
 * @author Gabriele Cipolloni
 */
package state;

/**
 * The Class Main.
 */
public class Main {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		Clock clock = new Clock();
		clock.modeButton();
		clock.changeButton();
		clock.modeButton();
		clock.changeButton();
		clock.modeButton();
		clock.changeButton();
	}
	
}
