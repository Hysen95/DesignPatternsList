/**
 * @author Gabriele Cipolloni
 */
package decorator;

/**
 * The Class Main.
 */
public class Main {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		IComponent component = new Component();
		System.out.println("\n ---------- \n");
		component.operation();
		System.out.println("\n ---------- \n");
		component = new ConcreteDecorator(component);
		component.operation();
		System.out.println("\n ---------- \n");
		component = new ConcreteDecorator(component);
		component.operation();
		System.out.println("\n ---------- \n");
	}

}
