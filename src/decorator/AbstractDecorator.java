/**
 * @author Gabriele Cipolloni
 */
package decorator;

/**
 * The Class AbstractDecorator.
 */
public abstract class AbstractDecorator implements IComponent {

	/** The component. */
	private IComponent component;

	/**
	 * Instantiates a new abstract decorator.
	 *
	 * @param component the component
	 */
	public AbstractDecorator(IComponent component) {
		this.component = component;
	}

	/**
	 * Gets the component.
	 *
	 * @return the component
	 */
	public IComponent getComponent() {
		return this.component;
	}

	/* (non-Javadoc)
	 * @see decorator.IComponent#operation()
	 */
	@Override
	public void operation() {
		this.component.operation();
		System.out.println("\n I am a Decorator. ");
	}

	/**
	 * Sets the component.
	 *
	 * @param component the new component
	 */
	public void setComponent(IComponent component) {
		this.component = component;
	}

}
