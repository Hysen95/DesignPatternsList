/**
 * @author Gabriele Cipolloni
 */
package decorator;

/**
 * The Interface IComponent.
 */
public interface IComponent {

	/**
	 * Operation.
	 */
	void operation();

}
