/**
 * @author Gabriele Cipolloni
 */
package decorator;

/**
 * The Class Component.
 */
public class Component implements IComponent {

	/* (non-Javadoc)
	 * @see decorator.IComponent#operation()
	 */
	@Override
	public void operation() {
		System.out.println("\n I am a Component. ");
	}

}
