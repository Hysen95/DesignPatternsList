/**
 * @author Gabriele Cipolloni
 */
package decorator;

/**
 * The Class ConcreteDecorator.
 */
public class ConcreteDecorator extends AbstractDecorator {

	/**
	 * Instantiates a new concrete decorator.
	 *
	 * @param component the component
	 */
	public ConcreteDecorator(IComponent component) {
		super(component);
	}

	/* (non-Javadoc)
	 * @see decorator.AbstractDecorator#operation()
	 */
	@Override
	public void operation() {
		super.operation();
		System.out.println("\n I am a ConcreteDecorator. ");
	}

}
