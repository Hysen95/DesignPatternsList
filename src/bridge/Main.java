/**
 * @author Gabriele Cipolloni
 */
package bridge;

/**
 * The Class Main.
 */
public class Main {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		RefinedAbstraction refinedAbstraction = new RefinedAbstraction(new API1());
		refinedAbstraction.operation1();
	}

}
