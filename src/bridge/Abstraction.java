/**
 * @author Gabriele Cipolloni
 */
package bridge;

/**
 * The Class Abstraction.
 */
public abstract class Abstraction {

	/** The api. */
	private API api;

	/**
	 * Instantiates a new abstraction.
	 *
	 * @param api the api
	 */
	protected Abstraction(API api) {
		this.api = api;
	}

	/**
	 * Gets the api.
	 *
	 * @return the api
	 */
	public API getApi() {
		return this.api;
	}

	/**
	 * Operation1.
	 */
	public abstract void operation1();

	/**
	 * Sets the api.
	 *
	 * @param api the new api
	 */
	public void setApi(API api) {
		this.api = api;
	}

}
