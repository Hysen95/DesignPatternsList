/**
 * @author Gabriele Cipolloni
 */
package bridge;

/**
 * The Interface API.
 */
public interface API {

	/**
	 * Operation1.
	 */
	void operation1();

}
