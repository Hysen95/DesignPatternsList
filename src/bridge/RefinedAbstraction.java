/**
 * @author Gabriele Cipolloni
 */
package bridge;

/**
 * The Class RefinedAbstraction.
 */
public class RefinedAbstraction extends Abstraction {

	/**
	 * Instantiates a new refined abstraction.
	 *
	 * @param api the api
	 */
	public RefinedAbstraction(API api) {
		super(api);
	}

	/* (non-Javadoc)
	 * @see bridge.Abstraction#operation1()
	 */
	@Override
	public void operation1() {
		this.getApi().operation1();
	}

}
