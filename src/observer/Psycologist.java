/**
 * @author Gabriele Cipolloni
 */
package observer;

import java.util.Observable;
import java.util.Observer;

/**
 * The Class Psycologist.
 */
public class Psycologist implements Observer {

	/* (non-Javadoc)
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */
	@Override
	public void update(Observable observable, Object object) {
		System.out.println("\n Psycologist - Osservato: "+((ObservedSubject)object).getValue()+".");
	}
	
}
