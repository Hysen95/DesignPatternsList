/**
 * @author Gabriele Cipolloni
 */
package observer;

/**
 * The Class Main.
 */
public class Main {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		ObservedSubject observedSubject = new ObservedSubject();
		Watcher watcher = new Watcher();
		Psycologist psycologist = new Psycologist();
		observedSubject.addObserver(watcher);
		observedSubject.addObserver(psycologist);
		for (int i=0; i<10; i++) {
			observedSubject.receiveValue(i);
		}
	}
	
}
