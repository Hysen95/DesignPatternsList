/**
 * @author Gabriele Cipolloni
 */
package observer;

import java.util.Observable;

/**
 * The Class ObservedSubject.
 */
public class ObservedSubject extends Observable {

	/** The value. */
	private int value;
	
	/**
	 * Receive value.
	 *
	 * @param value the value
	 */
	public void receiveValue(int value) {
		this.value = value;
		setChanged();
		notifyObservers(this);
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public int getValue() {
		return value;
	}
	
}
