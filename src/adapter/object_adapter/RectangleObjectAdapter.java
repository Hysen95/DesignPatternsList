/**
 * @author Gabriele Cipolloni
 */
package adapter.object_adapter;

/**
 * The Class RectangleObjectAdapter.
 */
public class RectangleObjectAdapter implements Polygon {

	/** The rectangle. */
	private Rectangle rectangle;
	
	/** The id. */
	private String id;

	/**
	 * Instantiates a new rectangle object adapter.
	 */
	public RectangleObjectAdapter() {
		this.rectangle = new Rectangle();
	}

	/* (non-Javadoc)
	 * @see adapter.object_adapter.Polygon#getCoordinates()
	 */
	@Override
	public float[] getCoordinates() {
		float[] coordinates = new float[4];
		coordinates[0] = this.rectangle.getOriginX();
		coordinates[1] = this.rectangle.getOriginY();
		coordinates[2] = this.rectangle.getOppositeCornerX();
		coordinates[3] = this.rectangle.getOppositeCornerY();
		return coordinates;
	}

	/* (non-Javadoc)
	 * @see adapter.object_adapter.Polygon#getId()
	 */
	@Override
	public String getId() {
		return this.id;
	}

	/* (non-Javadoc)
	 * @see adapter.object_adapter.Polygon#setId(java.lang.String)
	 */
	@Override
	public void setId(String id) {
		this.id = id;
	}

}
