/**
 * @author Gabriele Cipolloni
 */
package adapter.class_adapter;

/**
 * The Interface Polygon.
 */
public interface Polygon {

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	void setId(String id);
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	String getId();
	
	/**
	 * Gets the coordinates.
	 *
	 * @return the coordinates
	 */
	float[] getCoordinates();
	
}
