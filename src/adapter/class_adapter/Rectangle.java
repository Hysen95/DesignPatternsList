/**
 * @author Gabriele Cipolloni
 */
package adapter.class_adapter;

/**
 * The Class Rectangle.
 */
public class Rectangle {

	/** The height. */
	private float x, y, weight, height;
	
	/** The color. */
	private String color;
	
	/**
	 * Sets the shape.
	 *
	 * @param x the x
	 * @param y the y
	 * @param weight the weight
	 * @param height the height
	 * @param color the color
	 */
	public void setShape(float x, float y, float weight, float height, String color) {
		this.x = x;
		this.y = y;
		this.weight = weight;
		this.height = height;
		this.color = color;
	}
	
	/**
	 * Gets the area.
	 *
	 * @return the area
	 */
	public float getArea() {
		return x * y;
	}
	
	/**
	 * Gets the origin x.
	 *
	 * @return the origin x
	 */
	public float getOriginX() {
		return x;
	}
	
	/**
	 * Gets the origin y.
	 *
	 * @return the origin y
	 */
	public float getOriginY() {
		return y;
	}
	
	/**
	 * Gets the opposite corner x.
	 *
	 * @return the opposite corner x
	 */
	public float getOppositeCornerX() {
		return x + height;
	}
	
	/**
	 * Gets the opposite corner y.
	 *
	 * @return the opposite corner y
	 */
	public float getOppositeCornerY() {
		return y + weight;
	}
	
	/**
	 * Gets the color.
	 *
	 * @return the color
	 */
	public String getColor() {
		return color;
	}
	
}
