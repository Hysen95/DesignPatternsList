/**
 * @author Gabriele Cipolloni
 */
package adapter.class_adapter;

/**
 * The Class RectangleClassAdapter.
 */
public class RectangleClassAdapter extends Rectangle implements Polygon {

	/** The id. */
	private String id;
	
	/* (non-Javadoc)
	 * @see adapter.class_adapter.Polygon#setId(java.lang.String)
	 */
	@Override
	public void setId(String id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see adapter.class_adapter.Polygon#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see adapter.class_adapter.Polygon#getCoordinates()
	 */
	@Override
	public float[] getCoordinates() {
		float[] coordinates = new float[4];
		coordinates[0] = getOriginX();
		coordinates[1] = getOriginY();
		coordinates[2] = getOppositeCornerX();
		coordinates[3] = getOppositeCornerY();
		return coordinates;
	}
	
}
