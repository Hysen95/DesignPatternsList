/**
 * @author Gabriele Cipolloni
 */
package facade;

/**
 * The Class ConcreteItem.
 */
public class ConcreteItem implements Item {

	/* (non-Javadoc)
	 * @see facade.Item#operation()
	 */
	@Override
	public void operation() {

	}

}
