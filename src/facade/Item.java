/**
 * @author Gabriele Cipolloni
 */
package facade;

/**
 * The Interface Item.
 */
public interface Item {

	/**
	 * Operation.
	 */
	void operation();

}
