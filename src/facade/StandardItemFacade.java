/**
 * @author Gabriele Cipolloni
 */
package facade;

/**
 * The Class StandardItemFacade.
 */
public class StandardItemFacade extends AbstractItemFacade {

	/** The item. */
	private Item item;

	/**
	 * Instantiates a new standard item facade.
	 *
	 * @param item the item
	 */
	public StandardItemFacade(Item item) {
		this.item = item;
	}

	/* (non-Javadoc)
	 * @see facade.IFacade#operationOnItem()
	 */
	@Override
	public void operationOnItem() {
		System.out.println("\n Start Item Opertion. ");
		this.item.operation();
		System.out.println("\n End Item Opertion. ");
	}

}
