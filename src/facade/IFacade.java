/**
 * @author Gabriele Cipolloni
 */
package facade;

/**
 * The Interface IFacade.
 */
public interface IFacade {

	/**
	 * Operation on item.
	 */
	void operationOnItem();

}
