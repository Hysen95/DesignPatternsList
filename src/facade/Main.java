/**
 * @author Gabriele Cipolloni
 */
package facade;

/**
 * The Class Main.
 */
public class Main {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		IFacade facade = new StandardItemFacade(new ConcreteItem());
		facade.operationOnItem();
	}

}
