/**
 * @author Gabriele Cipolloni
 */
package chain_of_reponsability;

/**
 * The Class Client.
 */
public class Client {

	/**
	 * Request.
	 *
	 * @param abstractHandler the abstract handler
	 * @param param the param
	 * @throws AbstractHandlerException the abstract handler exception
	 */
	public void request(AbstractHandler abstractHandler, int param) throws AbstractHandlerException {
		abstractHandler.request(param);
	}
	
}
