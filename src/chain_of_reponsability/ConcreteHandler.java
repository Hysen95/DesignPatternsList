/**
 * @author Gabriele Cipolloni
 */
package chain_of_reponsability;

/**
 * The Class ConcreteHandler.
 */
public class ConcreteHandler extends AbstractHandler {

	/**
	 * Instantiates a new concrete handler.
	 *
	 * @param superior the superior
	 */
	public ConcreteHandler(AbstractHandler superior) {
		super(superior);
	}
	
	/**
	 * Instantiates a new concrete handler.
	 */
	public ConcreteHandler() {}
	
	/* (non-Javadoc)
	 * @see chain_of_reponsability.AbstractHandler#request(int)
	 */
	@Override
	public void request(int param) throws AbstractHandlerException {
		if (param > 1000) {
			super.forwardRequest(param);
		}
		else {
			System.out.println("\n OK: "+getClass().getSimpleName());
		}
	}
	
}
