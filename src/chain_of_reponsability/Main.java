/**
 * @author Gabriele Cipolloni
 */
package chain_of_reponsability;

/**
 * The Class Main.
 */
public class Main {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		ConcreteHandler concreteHandler = new ConcreteHandler();
		concreteHandler.setSuperior(new ConcreteHandlerTwo());
		Client client = new Client();
		int i = 2000;
		client.request(concreteHandler, i);
	}
	
}
