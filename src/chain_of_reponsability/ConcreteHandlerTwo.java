/**
 * @author Gabriele Cipolloni
 */
package chain_of_reponsability;

/**
 * The Class ConcreteHandlerTwo.
 */
public class ConcreteHandlerTwo extends AbstractHandler {

	/**
	 * Instantiates a new concrete handler two.
	 *
	 * @param superior the superior
	 */
	public ConcreteHandlerTwo(AbstractHandler superior) {
		super(superior);
	}
	
	/**
	 * Instantiates a new concrete handler two.
	 */
	public ConcreteHandlerTwo() {}
	
	/* (non-Javadoc)
	 * @see chain_of_reponsability.AbstractHandler#request(int)
	 */
	@Override
	public void request(int param) throws AbstractHandlerException {
		if (param > 2000) {
			super.forwardRequest(param);
		}
		else {
			System.out.println("\n OK: "+getClass().getSimpleName());
		}
	}
	
}
