/**
 * @author Gabriele Cipolloni
 */
package chain_of_reponsability;

/**
 * The Class AbstractHandler.
 */
public abstract class AbstractHandler {

	/** The superior. */
	private AbstractHandler superior;
	
	/**
	 * Instantiates a new abstract handler.
	 */
	public AbstractHandler() {}
	
	/**
	 * Instantiates a new abstract handler.
	 *
	 * @param superior the superior
	 */
	public AbstractHandler(AbstractHandler superior) {
		this.superior = superior;
	}
	
	/**
	 * Request.
	 *
	 * @param param the param
	 * @throws AbstractHandlerException the abstract handler exception
	 */
	public void request(int param) throws AbstractHandlerException {
		System.out.println("\n Param: "+param);
		forwardRequest(param);
	}
	
	/**
	 * Forward request.
	 *
	 * @param param the param
	 * @throws AbstractHandlerException the abstract handler exception
	 */
	protected void forwardRequest(int param) throws AbstractHandlerException {
		if (superior != null) {
			superior.request(param);
		}
		else {
//			throw new AbstractHandlerException("");
		}
	}

	/**
	 * Gets the superior.
	 *
	 * @return the superior
	 */
	public AbstractHandler getSuperior() {
		return superior;
	}
	
	/**
	 * Sets the superior.
	 *
	 * @param superior the new superior
	 */
	public void setSuperior(AbstractHandler superior) {
		this.superior = superior;
	}
	
}
