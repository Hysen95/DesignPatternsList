/**
 * @author Gabriele Cipolloni
 */
package chain_of_reponsability;

/**
 * The Class AbstractHandlerException.
 */
public class AbstractHandlerException extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new abstract handler exception.
	 *
	 * @param exception the exception
	 */
	public AbstractHandlerException(Exception exception) {
		super(exception);
	}
	
	/**
	 * Instantiates a new abstract handler exception.
	 *
	 * @param message the message
	 */
	public AbstractHandlerException(String message) {
		super(message);
	}
	
}
