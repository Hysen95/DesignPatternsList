/**
 * @author Gabriele Cipolloni
 */
package mediator;

/**
 * The Interface IMediator.
 */
public interface IMediator {

	/**
	 * Adds the colleague.
	 *
	 * @param colleague the colleague
	 */
	void addColleague(Colleague colleague);

	/**
	 * Send.
	 *
	 * @param message the message
	 * @param colleague the colleague
	 */
	void send(String message, Colleague colleague);

}
