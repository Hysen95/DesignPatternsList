/**
 * @author Gabriele Cipolloni
 */
package mediator;

/**
 * The Class Colleague.
 */
public abstract class Colleague implements IReceivable {

	/** The mediator. */
	private IMediator mediator;

	/**
	 * Instantiates a new colleague.
	 *
	 * @param mediator the mediator
	 */
	public Colleague(IMediator mediator) {
		this.mediator = mediator;
	}

	/**
	 * Gets the mediator.
	 *
	 * @return the mediator
	 */
	public IMediator getMediator() {
		return this.mediator;
	}

	/**
	 * Send.
	 *
	 * @param message the message
	 */
	public void send(String message) {
		this.mediator.send(message, this);
	}

	/**
	 * Sets the mediator.
	 *
	 * @param mediator the new mediator
	 */
	public void setMediator(IMediator mediator) {
		this.mediator = mediator;
	}

}
