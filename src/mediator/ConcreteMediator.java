/**
 * @author Gabriele Cipolloni
 */
package mediator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * The Class ConcreteMediator.
 */
public class ConcreteMediator implements IMediator {

	/** The colleagues. */
	private List<Colleague> colleagues;

	/**
	 * Instantiates a new concrete mediator.
	 */
	public ConcreteMediator() {
		this.colleagues = new ArrayList<>();
	}

	/* (non-Javadoc)
	 * @see mediator.IMediator#addColleague(mediator.Colleague)
	 */
	public void addColleague(Colleague colleague) {
		this.colleagues.add(colleague);
	}

	/* (non-Javadoc)
	 * @see mediator.IMediator#send(java.lang.String, mediator.Colleague)
	 */
	@Override
	public void send(String message, Colleague originator) {
		Iterator<Colleague> collegueIterator = this.colleagues.iterator();
		while (collegueIterator.hasNext()) {
			Colleague colleague = collegueIterator.next();
			if (colleague != originator) {
				colleague.receive(message);
			}
		}
	}

}
