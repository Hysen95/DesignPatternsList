/**
 * @author Gabriele Cipolloni
 */
package mediator;

/**
 * The Class Main.
 */
public class Main {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		IMediator mediator = new ConcreteMediator();
		Colleague one = new ConcreteColleague(mediator);
		Colleague two = new ConcreteColleague(mediator);
		mediator.addColleague(one);
		mediator.addColleague(two);
		one.send("c");
		two.send("cc");
	}

}
