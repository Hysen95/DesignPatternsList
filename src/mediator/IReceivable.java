/**
 * @author Gabriele Cipolloni
 */
package mediator;

/**
 * The Interface IReceivable.
 */
public interface IReceivable {

	/**
	 * Receive.
	 *
	 * @param message the message
	 */
	void receive(String message);

}
