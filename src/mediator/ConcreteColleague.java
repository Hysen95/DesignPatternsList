/**
 * @author Gabriele Cipolloni
 */
package mediator;

/**
 * The Class ConcreteColleague.
 */
public class ConcreteColleague extends Colleague {

	/**
	 * Instantiates a new concrete colleague.
	 *
	 * @param mediator the mediator
	 */
	public ConcreteColleague(IMediator mediator) {
		super(mediator);
	}

	/* (non-Javadoc)
	 * @see mediator.IReceivable#receive(java.lang.String)
	 */
	@Override
	public void receive(String message) {
		System.out.println("\n Colleague received: " + message);
	}

}
