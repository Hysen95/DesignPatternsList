/**
 * @author Gabriele Cipolloni
 */
package factory_method;

/**
 * A factory for creating Product objects.
 */
public abstract class ProductFactory implements IProductFactory {

	/* (non-Javadoc)
	 * @see factory_method.IProductFactory#createProduct()
	 */
	@Override
	public final AbstractProduct createProduct() {
		return this.newProduct();
	}

	/**
	 * New product.
	 *
	 * @return the abstract product
	 */
	protected abstract AbstractProduct newProduct();

}
