/**
 * @author Gabriele Cipolloni
 */
package factory_method;

/**
 * The Class Main.
 */
public class Main {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		IProductFactory creator = new ConcreteProductFactory();
		AbstractProduct product = creator.createProduct();
		System.out.println("\n AbstractProduct: " + product);
	}

}
