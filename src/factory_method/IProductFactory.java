/**
 * @author Gabriele Cipolloni
 */
package factory_method;

/**
 * A factory for creating IProduct objects.
 */
public interface IProductFactory {

	/**
	 * Creates a new IProduct object.
	 *
	 * @return the abstract product
	 */
	AbstractProduct createProduct();

}
