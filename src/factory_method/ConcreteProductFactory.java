/**
 * @author Gabriele Cipolloni
 */
package factory_method;

/**
 * A factory for creating ConcreteProduct objects.
 */
public class ConcreteProductFactory extends ProductFactory {

	/* (non-Javadoc)
	 * @see factory_method.ProductFactory#newProduct()
	 */
	@Override
	protected AbstractProduct newProduct() {
		return new ConcreteProduct();
	}

}
