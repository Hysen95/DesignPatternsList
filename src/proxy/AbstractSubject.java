/**
 * @author Gabriele Cipolloni
 */
package proxy;

/**
 * The Class AbstractSubject.
 */
public abstract class AbstractSubject {

	/** The name. */
	private String name;
	
	/**
	 * Instantiates a new abstract subject.
	 *
	 * @param name the name
	 */
	public AbstractSubject(String name) {
		this.name = name;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

}
