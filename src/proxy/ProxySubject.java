/**
 * @author Gabriele Cipolloni
 */
package proxy;

/**
 * The Class ProxySubject.
 */
public class ProxySubject extends AbstractSubject {

	/** The real subject. */
	private RealSubject realSubject;
	
	/** The name cache. */
	private String nameCache;
	
	/**
	 * Instantiates a new proxy subject.
	 *
	 * @param name the name
	 */
	public ProxySubject(String name) {
		super(name);
	}
	
	/**
	 * Operation.
	 */
	public void operation() {
		if (realSubject == null) {
			realSubject = new RealSubject("ciao");
			nameCache = realSubject.getName();
		}
		realSubject.operation();
	}

	/**
	 * Gets the name cache.
	 *
	 * @return the name cache
	 */
	public String getNameCache() {
		return nameCache;
	}
	
}
