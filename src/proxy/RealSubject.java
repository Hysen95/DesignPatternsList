/**
 * @author Gabriele Cipolloni
 */
package proxy;

/**
 * The Class RealSubject.
 */
public class RealSubject extends AbstractSubject {

	/**
	 * Instantiates a new real subject.
	 *
	 * @param name the name
	 */
	public RealSubject(String name) {
		super(name);
	}
	
	/**
	 * Operation.
	 */
	public void operation() {
		System.out.println(this);
	}
	
}
