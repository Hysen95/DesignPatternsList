/**
 * @author Gabriele Cipolloni
 */
package prototype;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * The Class Prototype.
 */
public abstract class Prototype implements Cloneable {

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Prototype clone() throws CloneNotSupportedException {
		Prototype prototype = (Prototype)super.clone();
		return prototype;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "\n " + ReflectionToStringBuilder.toString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	
}
