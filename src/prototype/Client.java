/**
 * @author Gabriele Cipolloni
 */
package prototype;

/**
 * The Class Client.
 */
public class Client {

	/**
	 * Clone prototype.
	 *
	 * @throws CloneNotSupportedException the clone not supported exception
	 */
	public void clonePrototype() throws CloneNotSupportedException {
		Prototype prototype = new ConcretePrototype();
		System.out.println("\n Prototype: "+prototype);
		Prototype prototypeClone = prototype.clone();
		System.out.println("\n PrototypeClone: "+prototypeClone);
	}
	
}
