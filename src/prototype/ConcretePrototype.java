/**
 * @author Gabriele Cipolloni
 */
package prototype;

/**
 * The Class ConcretePrototype.
 */
public class ConcretePrototype extends Prototype {

	/** The name. */
	private String name;

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
}
