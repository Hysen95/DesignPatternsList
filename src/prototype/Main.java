/**
 * @author Gabriele Cipolloni
 */
package prototype;

/**
 * The Class Main.
 */
public class Main {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		Client client = new Client();
		try {
			client.clonePrototype();
		}
		catch(CloneNotSupportedException cloneNotSupportedException) {
			cloneNotSupportedException.printStackTrace();
		}
	}
	
}
