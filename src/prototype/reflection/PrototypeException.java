/**
 * @author Gabriele Cipolloni
 */
package prototype.reflection;

/**
 * The Class PrototypeException.
 */
public class PrototypeException extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new prototype exception.
	 *
	 * @param message the message
	 */
	public PrototypeException(String message) {
		super(message);
	}
	
	/**
	 * Instantiates a new prototype exception.
	 *
	 * @param e the e
	 */
	public PrototypeException(Exception e) {
		super(e);
	}
	
}
