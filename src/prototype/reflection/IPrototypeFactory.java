/**
 * @author Gabriele Cipolloni
 */
package prototype.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

/**
 * A factory for creating IPrototype objects.
 */
public interface IPrototypeFactory {

	/**
	 * Clone.
	 *
	 * @param prototypeToClone the prototype to clone
	 * @return the i prototype
	 */
	static IPrototype clone(IPrototype prototypeToClone) {
		try {
			IPrototype prototypeClone = prototypeToClone.getClass().newInstance();
			for (Field field : prototypeToClone.getClass().getDeclaredFields()) {
				field.setAccessible(true);
				field.set(prototypeClone, field.get(prototypeToClone));
			}
			return prototypeClone;
		}
		catch (IllegalAccessException | InstantiationException e) {
			throw new PrototypeException(e);
		}
	}

	/**
	 * Gets the new prototype object.
	 *
	 * @param classType the class type
	 * @return the new prototype object
	 */
	static IPrototype getNewPrototypeObject(Class<IPrototype> classType) {
		try {
			Constructor<?> classConstructor = classType.getConstructor(new Class[] {});
			return (IPrototype) classConstructor.newInstance(new Object[] {});
		}
		catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
			throw new PrototypeException(e);
		}
	}

}
