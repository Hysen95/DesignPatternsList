/**
 * @author Gabriele Cipolloni
 */
package prototype.reflection;

/**
 * The Class Main.
 */
public class Main {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		Client client = new Client();
		try {
			client.operation();
		}
		catch(CloneNotSupportedException cloneNotSupportedException) {
			cloneNotSupportedException.printStackTrace();
		}
	}
	
}
