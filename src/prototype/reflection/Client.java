/**
 * @author Gabriele Cipolloni
 */
package prototype.reflection;

/**
 * The Class Client.
 */
public class Client {

	/**
	 * Operation.
	 *
	 * @throws CloneNotSupportedException the clone not supported exception
	 */
	public void operation() throws CloneNotSupportedException {
		IPrototype prototype = new ConcretePrototype();
		System.out.println("\n Prototype: "+prototype);
		IPrototype prototypeClone = IPrototypeFactory.clone(prototype);
		System.out.println("\n PrototypeClone: "+prototypeClone);
	}
	
}
