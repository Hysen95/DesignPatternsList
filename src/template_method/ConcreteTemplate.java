/**
 * @author Gabriele Cipolloni
 */
package template_method;

/**
 * The Class ConcreteTemplate.
 */
public class ConcreteTemplate implements ITemplate {

	/* (non-Javadoc)
	 * @see template_method.ITemplate#isValid(int)
	 */
	@Override
	public boolean isValid(int param) {
		return true;
	}
	
}
