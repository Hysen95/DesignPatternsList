/**
 * @author Gabriele Cipolloni
 */
package template_method;

/**
 * The Interface ITemplate.
 */
public interface ITemplate {

	/**
	 * Template method.
	 */
	default void templateMethod() {
		if (isValid(5)) {
			System.out.println(this);
		}
	}
	
	/**
	 * Checks if is valid.
	 *
	 * @param param the param
	 * @return true, if is valid
	 */
	boolean isValid(int param);
	
}
