/**
 * @author Gabriele Cipolloni
 */
package template_method;

/**
 * The Class Main.
 */
public class Main {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		ITemplate abstractClass = new ConcreteTemplate();
		abstractClass.templateMethod();
	}

}
