/**
 * @author Gabriele Cipolloni
 */
package abstract_factory.util;

/**
 * The Enum ProductFactoryConstants.
 */
public enum ProductFactoryConstants {

	/** The pizza. */
	PIZZA, /** The kebab. */
 KEBAB;

}
