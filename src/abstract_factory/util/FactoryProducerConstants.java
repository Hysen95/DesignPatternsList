/**
 * @author Gabriele Cipolloni
 */
package abstract_factory.util;

/**
 * The Enum FactoryProducerConstants.
 */
public enum FactoryProducerConstants {

	/** The product. */
	PRODUCT;

}
