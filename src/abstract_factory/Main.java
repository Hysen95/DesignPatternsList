/**
 * @author Gabriele Cipolloni
 */
package abstract_factory;

import abstract_factory.util.FactoryProducerConstants;
import abstract_factory.util.ProductFactoryConstants;

/**
 * The Class Main.
 */
public class Main {

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		IFactory factory = FactoryProducer.getFactory(FactoryProducerConstants.PRODUCT);
		IProduct product = factory.getProduct(ProductFactoryConstants.PIZZA);
		System.out.println(product);
	}

}
