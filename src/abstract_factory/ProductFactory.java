/**
 * @author Gabriele Cipolloni
 */
package abstract_factory;

import abstract_factory.util.ProductFactoryConstants;

/**
 * A factory for creating Product objects.
 */
public class ProductFactory implements IFactory {

	/* (non-Javadoc)
	 * @see abstract_factory.IFactory#getProduct(abstract_factory.ProductFactoryConstants)
	 */
	@Override
	public IProduct getProduct(ProductFactoryConstants constant) {
		switch (constant) {
			case PIZZA: {
				return new Pizza();
			}
			case KEBAB:
			default: {
				return new Kebab();
			}
		}
	}

}
