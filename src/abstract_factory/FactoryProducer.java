/**
 * @author Gabriele Cipolloni
 */
package abstract_factory;

import abstract_factory.util.FactoryProducerConstants;

/**
 * The Class FactoryProducer.
 */
public class FactoryProducer {

	/**
	 * Gets the factory.
	 *
	 * @param constant the constant
	 * @return the factory
	 */
	public static IFactory getFactory(FactoryProducerConstants constant) {
		switch (constant) {
			case PRODUCT: {
				return new ProductFactory();
			}
			default: {
				throw new IllegalArgumentException("Null constant");
			}
		}
	}

	/**
	 * Instantiates a new factory producer.
	 */
	private FactoryProducer() {

	}

}
