/**
 * @author Gabriele Cipolloni
 */
package abstract_factory;

import abstract_factory.util.ProductFactoryConstants;

/**
 * A factory for creating I objects.
 */
public interface IFactory {

	/**
	 * Gets the product.
	 *
	 * @param constant the constant
	 * @return the product
	 */
	IProduct getProduct(ProductFactoryConstants constant);

}
