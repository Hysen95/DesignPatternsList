/**
 * @author Gabriele Cipolloni
 */
package builder;

/**
 * The Class Entity.
 */
public class Entity {

	/**
	 * The Class Builder.
	 */
	public static class Builder implements IBuilder<Entity> {

		/** The entity. */
		private Entity entity;

		/**
		 * Instantiates a new builder.
		 */
		public Builder() {
			this.entity = new Entity();
		}

		/* (non-Javadoc)
		 * @see builder.IBuilder#build()
		 */
		@Override
		public Entity build() {
			return this.entity;
		}

		/**
		 * Id.
		 *
		 * @param id the id
		 * @return the builder
		 */
		public Builder id(String id) {
			this.entity.id = id;
			return this;
		}

	}

	/** The id. */
	private String id;

	/**
	 * Instantiates a new entity.
	 */
	private Entity() {
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return this.id;
	}

}
