/**
 * @author Gabriele Cipolloni
 */
package builder;

/**
 * The Interface IBuilder.
 *
 * @param <T> the generic type
 */
public interface IBuilder<T> {

	/**
	 * Builds the.
	 *
	 * @return the t
	 */
	T build();

}
