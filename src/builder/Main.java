/**
 * @author Gabriele Cipolloni
 */
package builder;

/**
 * The Class Main.
 */
public class Main {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		Entity.Builder builder = new Entity.Builder();
		builder.id("c");
		Entity entity = builder.build();
		System.out.println(entity.getId());
	}

}
