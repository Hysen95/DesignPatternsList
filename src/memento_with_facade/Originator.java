/**
 * @author Gabriele Cipolloni
 */
package memento_with_facade;

/**
 * The Class Originator.
 */
public class Originator implements Cloneable, IOriginator {

	/**
	 * The Class Memento.
	 */
	private class Memento {

		/** The originator. */
		private Originator originator;

		/**
		 * Instantiates a new memento.
		 *
		 * @param originator the originator
		 * @throws CloneNotSupportedException the clone not supported exception
		 */
		public Memento(Originator originator) throws CloneNotSupportedException {
			this.originator = (Originator) originator.clone();
		}

		/**
		 * Gets the originator.
		 *
		 * @return the originator
		 */
		public Originator getOriginator() {
			return this.originator;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "Memento [originator=" + this.originator + "]";
		}

	}

	/** The id. */
	private String id;

	/**
	 * Instantiates a new originator.
	 */
	public Originator() {
	}

	/* (non-Javadoc)
	 * @see memento_with_facade.IOriginator#getId()
	 */
	@Override
	public String getId() {
		return this.id;
	}

	/* (non-Javadoc)
	 * @see memento_with_facade.IOriginator#restore(java.lang.Object)
	 */
	@Override
	public void restore(Object mementoObject) {
		if (mementoObject instanceof Memento) {
			Memento memento = (Memento) mementoObject;
			this.id = memento.getOriginator().getId();
		}
	}

	/* (non-Javadoc)
	 * @see memento_with_facade.IOriginator#save()
	 */
	@Override
	public Memento save() {
		try {
			return new Memento(this);
		}
		catch (CloneNotSupportedException cnse) {
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see memento_with_facade.IOriginator#setId(java.lang.String)
	 */
	@Override
	public void setId(String id) {
		this.id = id;
	}

}
