/**
 * @author Gabriele Cipolloni
 */
package memento_with_facade;

/**
 * The Interface IOriginator.
 */
public interface IOriginator {

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	String getId();

	/**
	 * Restore.
	 *
	 * @param object the object
	 */
	void restore(Object object);

	/**
	 * Save.
	 *
	 * @return the object
	 * @throws RuntimeException the runtime exception
	 */
	Object save() throws RuntimeException;

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	void setId(String id);

}
