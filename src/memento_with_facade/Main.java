/**
 * @author Gabriele Cipolloni
 */
package memento_with_facade;

/**
 * The Class Main.
 */
public class Main {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		IOriginator originator = IOriginatorFactory.getOriginator();
		originator.setId("10");
		Object o = originator.save();
		originator.setId("11");
		System.out.println(originator.getId());
		originator.restore(o);
		System.out.println(originator.getId());
	}

}
