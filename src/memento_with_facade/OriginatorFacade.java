/**
 * @author Gabriele Cipolloni
 */
package memento_with_facade;

/**
 * The Class OriginatorFacade.
 */
public class OriginatorFacade implements IOriginator {

	/** The originator. */
	private Originator originator;

	/**
	 * Instantiates a new originator facade.
	 *
	 * @param originator the originator
	 */
	public OriginatorFacade(Originator originator) {
		this.originator = originator;
	}

	/* (non-Javadoc)
	 * @see memento_with_facade.IOriginator#getId()
	 */
	@Override
	public String getId() {
		return this.originator.getId();
	}

	/* (non-Javadoc)
	 * @see memento_with_facade.IOriginator#restore(java.lang.Object)
	 */
	@Override
	public void restore(Object memento) {
		this.originator.restore(memento);
	}

	/* (non-Javadoc)
	 * @see memento_with_facade.IOriginator#save()
	 */
	@Override
	public Object save() throws RuntimeException {
		Object memento = this.originator.save();
		if (memento == null) {
			throw new RuntimeException("Save failed");
		}
		else {
			return memento;
		}
	}

	/* (non-Javadoc)
	 * @see memento_with_facade.IOriginator#setId(java.lang.String)
	 */
	@Override
	public void setId(String id) {
		this.originator.setId(id);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.originator + "";
	}

}
