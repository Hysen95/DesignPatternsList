/**
 * @author Gabriele Cipolloni
 */
package memento_with_facade;

/**
 * A factory for creating IOriginator objects.
 */
public interface IOriginatorFactory {

	/**
	 * Gets the originator.
	 *
	 * @return the originator
	 */
	static IOriginator getOriginator() {
		return new OriginatorFacade(new Originator());
	}

}
