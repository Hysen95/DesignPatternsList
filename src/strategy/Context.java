/**
 * @author Gabriele Cipolloni
 */
package strategy;

/**
 * The Class Context.
 */
public class Context {

	/** The strategy. */
	private Strategy strategy;
	
	/**
	 * Instantiates a new context.
	 *
	 * @param strategy the strategy
	 */
	public Context(Strategy strategy) {
		this.strategy = strategy;
	}
	
	/**
	 * Execute strategy.
	 *
	 * @param x the x
	 */
	public void executeStrategy(int x) {
		strategy.execute(x);
	}

	/**
	 * Gets the strategy.
	 *
	 * @return the strategy
	 */
	public Strategy getStrategy() {
		return strategy;
	}

	/**
	 * Sets the strategy.
	 *
	 * @param strategy the new strategy
	 */
	public void setStrategy(Strategy strategy) {
		this.strategy = strategy;
	}
	
}
