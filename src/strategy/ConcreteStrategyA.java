/**
 * @author Gabriele Cipolloni
 */
package strategy;

/**
 * The Class ConcreteStrategyA.
 */
public class ConcreteStrategyA implements Strategy {

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName();
	}
	
	/* (non-Javadoc)
	 * @see strategy.Strategy#execute(int)
	 */
	@Override
	public void execute(int x) {
		System.out.println(toString() + " X:" + x);
	}
	
}
