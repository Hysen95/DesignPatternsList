/**
 * @author Gabriele Cipolloni
 */
package strategy;

/**
 * The Class Main.
 */
public class Main {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		Context context = new Context(new ConcreteStrategyA());
		context.executeStrategy(1);
		context.setStrategy(new ConcreteStrategyB());
		context.executeStrategy(1);
	}
	
}
