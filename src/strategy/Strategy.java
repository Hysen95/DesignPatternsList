/**
 * @author Gabriele Cipolloni
 */
package strategy;

/**
 * The Interface Strategy.
 */
public interface Strategy {

	/**
	 * Execute.
	 *
	 * @param x the x
	 */
	void execute(int x);
	
}
