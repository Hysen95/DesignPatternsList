/**
 * @author Gabriele Cipolloni
 */
package composite;

/**
 * The Class LeafException.
 */
public class LeafException extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new leaf exception.
	 *
	 * @param exception the exception
	 */
	public LeafException(Exception exception) {
		super(exception);
	}
	
	/**
	 * Instantiates a new leaf exception.
	 *
	 * @param message the message
	 */
	public LeafException(String message) {
		super(message);
	}
	
}
