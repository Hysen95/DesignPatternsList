/**
 * @author Gabriele Cipolloni
 */
package composite;

/**
 * The Interface IComponent.
 */
public interface IComponent {

	/**
	 * Adds the.
	 *
	 * @param component the component
	 * @throws LeafException the leaf exception
	 */
	public void add(IComponent component) throws LeafException;
	
	/**
	 * Removes the.
	 *
	 * @param component the component
	 * @throws LeafException the leaf exception
	 */
	public void remove(IComponent component) throws LeafException;
	
	/**
	 * Gets the child.
	 *
	 * @param index the index
	 * @return the child
	 * @throws LeafException the leaf exception
	 */
	public IComponent getChild(int index) throws LeafException;
	
}
