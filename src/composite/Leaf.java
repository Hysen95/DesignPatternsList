/**
 * @author Gabriele Cipolloni
 */
package composite;

/**
 * The Class Leaf.
 */
public class Leaf implements IComponent {

	/* (non-Javadoc)
	 * @see composite.IComponent#add(composite.IComponent)
	 */
	@Override
	public void add(IComponent component) throws LeafException {
		throw new LeafException("");
	}

	/* (non-Javadoc)
	 * @see composite.IComponent#remove(composite.IComponent)
	 */
	@Override
	public void remove(IComponent component) throws LeafException {
		throw new LeafException("");
	}

	/* (non-Javadoc)
	 * @see composite.IComponent#getChild(int)
	 */
	@Override
	public IComponent getChild(int index) throws LeafException {
		throw new LeafException("");
	}
	
}
