/**
 * @author Gabriele Cipolloni
 */
package composite;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class Composite.
 */
public class Composite implements IComponent {

	/** The components list. */
	private List<IComponent> componentsList;
	
	/**
	 * Instantiates a new composite.
	 */
	public Composite() {
		componentsList = new ArrayList<IComponent>(0);
	}
	
	/* (non-Javadoc)
	 * @see composite.IComponent#add(composite.IComponent)
	 */
	@Override
	public void add(IComponent component) {
		componentsList.add(component);
	}

	/* (non-Javadoc)
	 * @see composite.IComponent#remove(composite.IComponent)
	 */
	@Override
	public void remove(IComponent component) {
		componentsList.remove(component);
	}

	/* (non-Javadoc)
	 * @see composite.IComponent#getChild(int)
	 */
	@Override
	public IComponent getChild(int index) {
		return componentsList.get(index);
	}
	
}
