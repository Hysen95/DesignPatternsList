/**
 * @author Gabriele Cipolloni
 */
package composite;

/**
 * The Class Main.
 */
public class Main {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		IComponent leaf = new Leaf();
		IComponent main = new Composite();
		try {
			main.add(leaf); // OK.
			leaf.add(main); // Exception.
		}
		catch (LeafException leafException) {
			leafException.printStackTrace();
		}
	}
	
}
