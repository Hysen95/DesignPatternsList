/**
 * @author Gabriele Cipolloni
 */
package singleton.multithread.final_class;

//-	Problemi risolti:
//- 		Presenza di multiple istanze come sottoclassi di un Singleton;

/**
 * The Class Singleton.
 */
public final class Singleton {

	/** The instance. */
	private static Singleton instance;
	
	/**
	 * Instantiates a new singleton.
	 */
	private Singleton() {}
	
	/**
	 * Gets the single instance of Singleton.
	 *
	 * @return single instance of Singleton
	 */
	public static synchronized Singleton getInstance() {
		if (instance == null) {
			instance = new Singleton();
		}
		return instance;
	}
	
	/**
	 * Prints the.
	 *
	 * @param message the message
	 */
	public void print(String message) {
		System.out.println(message);
	}
	
}
