/**
 * @author Gabriele Cipolloni
 */
package singleton.multithread;

//-	Problemi possibili:
//- 		Presenza di Singletons in multiple virtual machines;
//- 		Singletons caricati contemporaneamente da diversi class loaders;
//- 		Singletons distrutti dal garbage collector, e dopo ricaricati quando sono necessari;
//- 		Presenza di multiple istanze come sottoclassi di un Singleton;
//- 		Copia di Singletons come risultato di un doppio processo di deserializzazione;
//-			---> Non implementare "Serializable".

/**
 * The Class Singleton.
 */
public class Singleton {

	/** The instance. */
	private static Singleton instance;
	
	/**
	 * Instantiates a new singleton.
	 */
	private Singleton() {}
	
	/**
	 * Gets the single instance of Singleton.
	 *
	 * @return single instance of Singleton
	 */
	public static synchronized Singleton getInstance() {
		if (instance == null) {
			instance = new Singleton();
		}
		return instance;
	}
	
	/**
	 * Prints the.
	 *
	 * @param message the message
	 */
	public void print(String message) {
		System.out.println(message);
	}
	
}
