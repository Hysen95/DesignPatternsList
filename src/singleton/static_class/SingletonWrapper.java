/**
 * @author Gabriele Cipolloni
 */
package singleton.static_class;

/**
 * The Class SingletonWrapper.
 */
public class SingletonWrapper {
	
	/**
	 * Instantiates a new singleton wrapper.
	 */
	private SingletonWrapper() {}
	
	/**
	 * The Class Singleton.
	 */
	public static class Singleton {
		
		/**
		 * Instantiates a new singleton.
		 */
		private Singleton() {}
		
		/**
		 * Prints the.
		 *
		 * @param message the message
		 */
		public static void print(String message) {
			System.out.println(message);
		}
		
	}
	
}
