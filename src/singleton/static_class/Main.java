/**
 * @author Gabriele Cipolloni
 */
package singleton.static_class;

/**
 * The Class Main.
 */
public class Main {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SingletonWrapper.Singleton.print("ciao");
	}
	
}
