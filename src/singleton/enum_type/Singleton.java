/**
 * @author Gabriele Cipolloni
 */
package singleton.enum_type;

/**
 * The Enum Singleton.
 */
public enum Singleton {

	/** The instance. */
	INSTANCE;

}
