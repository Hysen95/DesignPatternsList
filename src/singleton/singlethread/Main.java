/**
 * @author Gabriele Cipolloni
 */
package singleton.singlethread;

/**
 * The Class Main.
 */
public class Main {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		Singleton singleton = Singleton.getInstance();
		singleton.print("ciao");
	}
	
}
