/**
 * @author Gabriele Cipolloni
 */
package singleton.singlethread;

/**
 * The Class Singleton.
 */
public class Singleton {

	/** The instance. */
	private static Singleton instance;
	
	/**
	 * Instantiates a new singleton.
	 */
	private Singleton() {}
	
	/**
	 * Gets the single instance of Singleton.
	 *
	 * @return single instance of Singleton
	 */
	public static Singleton getInstance() {
		if (instance == null) {
			instance = new Singleton();
		}
		return instance;
	}
	
	/**
	 * Prints the.
	 *
	 * @param message the message
	 */
	public void print(String message) {
		System.out.println(message);
	}
	
}
